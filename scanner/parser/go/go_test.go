package gosum

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestParse(t *testing.T) {
	types := []string{"small", "big", "duplicates", "malformed", "incompatible"}
	for _, tc := range types {
		t.Run(tc, func(t *testing.T) {
			fixture := testutil.Fixture(t, tc, "go.sum")
			got, _, err := Parse(fixture, parser.Options{})
			require.NoError(t, err)
			testutil.RequireExpectedPackages(t, tc, got)
		})
	}
}
